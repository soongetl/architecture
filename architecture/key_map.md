## Key Maps

A key map maintains the relationship between source records and destination records based on their unique keys.

```
interface KeyMapInterface extends \Countable
{

    /**
     * Configuration keys for specifying the key names for the extractor
     * and the loader.
     */
    const CONFIGURATION_EXTRACTOR_KEYS = 'extractor_keys';
    const CONFIGURATION_LOADER_KEYS = 'loader_keys';

    /**
     * Persist the mapping of an extracted key to the corresponding loaded key.
     *
     * @param array $extractedKey
     *   Extracted key values, keyed by key names.
     *
     * @param array $loadedKey
     *   Loaded key values, keyed by key names.
     */
    public function saveKeyMap(array $extractedKey, array $loadedKey) : void;

    /**
     * Retrieve the loaded key corresponding to a given extracted key.
     *
     * @param array $extractedKey
     *   Extracted key values, keyed by key names.
     *
     * @return array
     *   Loaded key values, keyed by key names.
     */
    public function lookupLoadedKey(array $extractedKey) : array;

    /**
     * Retrieve any extracted keys mapped to a given loaded key.
     *
     * Note that multiple extracted keys may map to one loaded key.
     *
     * @param array $loadedKey
     *   Loaded key values, keyed by key names.
     *
     * @return array[]
     *   Array of extracted keys, each of which is keyed by key names.
     */
    public function lookupExtractedKeys(array $loadedKey) : array;

    /**
     * Remove the mapping for a given extracted key from the map.
     *
     * @param array $extractedKey
     *   Extracted key values, keyed by key names.
     */
    public function delete(array $extractedKey) : void;

    /**
     * Iterate over the key map, generating the keys.
     *
     * @todo: Make the class \Iterable instead?
     *
     * @return iterable
     */
    public function iterate() : iterable;
}
```
