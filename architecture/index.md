# Soong ETL architecture

Note: the starting point for this document is pretty much a dump of the interfaces/classes from a PHP proof-of-concept - it still needs much organizing and fleshing out of supporting prose (beyond, of course, further development of the architecture being documented). See [the issue queue][162ae00a].

## [Data Abstractions][efc6f934]
## [Extractors][5d15d74e]
## [Loaders][0d423129]
## [Transformers][dedd5024]
## [Key Maps][c64968cb]
## [Tasks][c70c43cd]

  [162ae00a]: https://gitlab.com/soongetl/architecture/issues "Soong ETL architecture issue queue"
  [efc6f934]: ./data_abstractions.md "Data Abstractions"
  [5d15d74e]: ./extractor.md "Extractors"
  [0d423129]: ./loader.md "Loaders"
  [dedd5024]: ./transformer.md "Transformers"
  [c64968cb]: ./key_map.md "Key Maps"
  [c70c43cd]: ./task.md "Tasks"
