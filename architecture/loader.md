## Loaders

Loaders accept one `DataRecordInterface` instance at a time and load the data in contains into a destination as configured.

Note that not all destinations may permit deleting loaded data (e.g., a loader could be used to output a CSV file). The deletion capability (used by rollback operations) should be moved to a separate interface.

(much more to be fleshed out here)

```
interface LoaderInterface
{

    /**
     * @todo This needs to communicate disposition (success, failure) and key of result.
     *
     * @param \Soong\Data\DataRecordInterface $data
     * @return mixed
     */
    public function load(DataRecordInterface $data) : void;

    // @todo put on separate interface (shared with extractors)
    public function getProperties() : array;
    public function getKeyProperties() : array;

    /**
     * Remove a record which has been loaded from the destination.
     *
     * @todo move to RollbackableInterface
     *
     * @param array $key
     */
    public function delete(array $key) : void;
}
```
