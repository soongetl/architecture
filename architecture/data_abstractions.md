## Data abstractions

The ETL pipeline components need to communicate the data they handle with each other (extractor outputs need to pass through a series of transformers and ultimately into a loader). The canonical representation of such data would be an associative array of arbitrarily-typed values, but rather than require a single representation it is more flexible to abstract the data.

### DataPropertyInterface

Data properties are represented by an instance of `DataPropertyInterface`. Implementations of DataPropertyInterface should be immutable - their value should be set at construction time and may not subsequently be changed. The value may be any scalar, array, or object type - including `DataPropertyInterface`.

```
interface DataPropertyInterface
{

    /**
     * Return the property value.
     *
     * @return mixed
     */
    public function getValue();
}
```

### DataRecordInterface
A data record (a set of named `DataPropertyInterface` instances) is represented by `DataRecordInterface`. In the context of an ETL pipeline, an extractor will output a `DataRecordInterface` to input to transformers, and the transformation process will populate another instance of DataRecordInterface one property at a time to ultimately pass to a loader.
```
interface DataRecordInterface
{

    /**
     * Populate the record with a set of named data properties.
     * @todo Probably should be set in a constructor?
     *
     * @param array $data
     *   Associative array of property values, keyed by property name.
     */
    public function fromArray(array $data) : void;

    /**
     * Fetch the list of named properties as an associative array.
     * @todo Probably not necessary - this class should probably be an iterator.
     *
     * @return array
     *   Associative array of property values, keyed by property name.
     */
    public function toArray() : array;

    /**
     * Set a property from an existing DataPropertyInterface object.
     *
     * @param string $propertyName
     *   Name of the property to set.
     *
     * @param null|\Soong\Data\DataPropertyInterface $propertyData
     *   Property value to set.
     */
    public function setProperty(string $propertyName, ?DataPropertyInterface $propertyData) : void;

    /**
     * Retrieve a property value as a DataPropertyInterface.
     *
     * @param string $propertyName
     *   Name of the property to get.
     *
     * @return null|\Soong\Data\DataPropertyInterface
     *   Value of the property.
     */
    public function getProperty(string $propertyName) : ?DataPropertyInterface;
}

```
