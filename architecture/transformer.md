## Transformers

A transfomer accepts a data property value and returns another data property value derived according to its configuration.

```
interface TransformerInterface
{
    /**
     * Accept a data property and turn it into another data property.
     *
     * @param array $configuration
     *   Array of configuration options keyed by option name.
     *   @todo Should be passed in constructor, $data should be only parameter
     * @param \Soong\Data\DataPropertyInterface $data
     *   Property containing data to be transformed.
     *
     * @return \Soong\Data\DataPropertyInterface
     *   The transformed data.
     */
    public function transform(array $configuration, ?DataPropertyInterface $data) : ?DataPropertyInterface;
}
```
