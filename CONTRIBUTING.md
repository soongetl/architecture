# Contributing

Contributions are **welcome** and will be fully **credited**.

We accept contributions via Pull Requests on [Gitlab](https://gitlab.com/soong/architecture).


## Pull Requests

- **Document any change in architecture** - Make sure the `README.md`, `CHANGELOG.md`, and any other relevant documentation are kept up-to-date.

- **Send coherent history** - Make sure each individual commit in your pull request is meaningful. If you had to make multiple intermediate commits while developing, please [squash them](http://www.git-scm.com/book/en/v2/Git-Tools-Rewriting-History#Changing-Multiple-Commit-Messages) before submitting.
