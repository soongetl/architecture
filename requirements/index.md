# Soong ETL requirements

This will describe the functionality expected of a complete ETL system. Its starting point is based on many years of experience migrating data/content into Drupal and the tools and features we've found helpful, but of course other experiences are welcome to help inform the Soong ETL architecture. At the moment it's a semi-random list of features - it needs organization.

## Framework functionality

### Task management

A "task" (process for migrating a specific type of data) may have "hard" and "soft" dependencies on other tasks. If a "hard" dependency is not "complete" (i.e., not all source records are recorded as having been processed), the task should not run unless an explicit "force" option is provided - a "soft" dependency will not prevent a task from running.

The execution of a group of tasks will be ordered according to the task dependencies.

### migrate operation

By default, executing the `migrate` operation:

1. Processes each source record non previously processed.
2. For each source record, populates a destination record by executing a sequence of 1 or more transformers.
3. Loads each such record into a configurated destination.

Optionally the above processing may be modified when invoked:

* The processing may be limited to a maximum total number of records, processing the "first" X items and exiting.
* The processing may be limited to an explicit list of source records.
* The processing may include remigration of previously processed source records, overwriting any previously-created destination records entirely (i.e., the source data is considered the system-of-record).
* The processing may include remigration of any source records whose contents are identified as having changed.
    * Changes may be identified by a "highwater mark", such as a last-modified timestamp, or
    * Changes may be identified by maintaining a hash of the migratable data.

Upon completion, results should be reported:
* The number of records processed.
* The number of records successfully migrated.
* The number of records which failed to migrate.
* The number of records which were explicitly ignored.

Optionally, the operation should support reporting the above statistics periodically during the operation, either every X records or every X seconds.

Errors (e.g. unexpected data values) should be recorded in such a way that the relevant source record(s) may be identified.

### rollback operation

By default, executing the `rollback` operation removes all loaded data from the destination store.

However, particular records may be flagged to not be removed (in particular, if a migration operation has updated pre-existing records rather than created them from scratch).

Optionally the above processing may be modified when invoked:

* The processing may be limited to a maximum total number of records, processing the "first" X items and exiting.
* The processing may be limited to an explicit list of source records.

Upon completion, results should be reported:
* The number of records processed.
* The number of records successfully removed.
* The number of records which failed to remove.
* The number of records which were explicitly ignored.

Optionally, the operation should support reporting the above statistics periodically during the operation, either every X records or every X seconds.

Applying the `rollback` operation to a group of tasks should execute them in reverse dependency order.

### status operation
The status of ETL tasks may be queried and displayed. Information which may be obtained includes:

* What operation (if any) is currently running for the task.
* If the source is countable, the total number of available records in the source.
* If task processing is being tracked, the total number of records which have been migrated.
* If the source is countable and task processing is being tracked, the number of records which have not been processed.
* The time of the last migration operation.

### analyze operation
The `analyze` operation provides information on available source data. Per-property information which may be provided includes:

* Whether the property is ever empty, or always empty.
* Apparent type of property values (integer, string, etc.).
* Range of property values.
* Range of string lengths where appropriate.
* Whether the property appears to be a select list (i.e., has a small number of distinct values).

### stop operation

The `stop` operation will cause any other non-status operation on that task to cleanly exit after processing the current record.

## Tools

The framework should support CLI and UI tools for executing ETL operations and managing tasks, as well as UI tools for editing task configuration.

to be continued...
