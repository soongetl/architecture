# Soong ETL project requirements and architecture.

[![Software License][ico-license]](LICENSE.md)

This repository documents:

1. Functionality which the ETL architecture should be capable of supporting.
2. An object-oriented architecture supporting that functionality.

The requirements and architecture should be language-neutral - for historical reasons, any code examples will be in PHP.

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.

## Credits

- [Mike Ryan][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[link-author]: https://gitlab.com/mikeryan776
[link-contributors]: ../../contributors
